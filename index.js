//Creaciones de los array para los respectivos Objetos:Examen, Pregunta, Insumo de Evaluación
var Examen =[
 
    {  id_Examen:1, Nombre_de_la_asignatura:"Sistemas Digitales"},  
    {  id_Examen:2, Nombre_de_la_asignatura:"Web II"},
    {  id_Examen:3, Nombre_de_la_asignatura:"Matematicas" },
    {  id_Examen:4, Nombre_de_la_asignatura:"Lenguaje"},
    {  id_Examen:5, Nombre_de_la_asignatura:"Ingenieria de Software"}
]; 
var Pregunta =[

    {   id_Pregunta:1, Pregunta: "Clasificacion de sistema", Categoria:"Sistemas digitales", Respuesta_correcta:"a"},      
    {   id_Pregunta:2, Pregunta: "que es programacion", Categoria:"Web II", Respuesta_correcta:"d"},
    {   id_Pregunta:3, Pregunta: "resuelva el siguiente ejercicio", Categoria:"Matematicas", Respuesta_correcta:"a"},
    {   id_Pregunta:4, Pregunta: "que es texto literario", Categoria:"Lenguaje", Respuesta_correcta:"b"},
    {   id_Pregunta:5, Pregunta: "metodos de ingenieria de software", Categoria:"Ingenieria de Software", Respuesta_correcta:"c"}
];


var Insumo_de_Evaluación =[

    {   id_Insumo_de_Evaluación:1, id_Pregunta:1, id_Examen:1, Valor:"9"},      
    {   id_Insumo_de_Evaluación:2, id_Pregunta:2, id_Examen:2, Valor:"10"},
    {   id_Insumo_de_Evaluación:3, id_Pregunta:3, id_Examen:3, Valor:"8"},
    {   id_Insumo_de_Evaluación:4, id_Pregunta:4, id_Examen:4, Valor:"7"},
    {   id_Insumo_de_Evaluación:5, id_Pregunta:5, id_Examen:5, Valor:"9"}
];

// Y utilizacion de .length para la distancia de los array de ambos caso tanto para Insumo de Evaluacion y Examen.
let conteo=0;
console.log('\n\n Examen');
while (conteo<Examen.length){

    console.log("ID del Examen : "+Examen[conteo].id_Examen+"  Nombre de la asignatura: "+Examen[conteo].Nombre_de_la_asignatura);
    conteo++;

}

console.log('\n\n Insumo_de_Evaluación');
for (let i=0; i<Insumo_de_Evaluación.length;i++){
    console.log('id Insumo_de_Evaluación: '+Insumo_de_Evaluación[i].id_Insumo_de_Evaluación+ '  id_Pregunta:  '+Insumo_de_Evaluación[i].id_Pregunta +'  id_Examen:  '+Insumo_de_Evaluación[i].id_Examen +
    '  valor:  '+Insumo_de_Evaluación[i].Valor);
}
console.log('\n\n Pregunta');

//Uso de un forEach que apartir de el elemento "AtributoActual" hacemos el llamado de cada uno de las propiedades dentro del array Pregunta."
Pregunta.forEach(Atributo_Actual => console.log('id_Pregunta: ' +Atributo_Actual.id_Pregunta +'  categoria '+Atributo_Actual.Categoria
+'    Respuesta correcta : '+Atributo_Actual.Respuesta_correcta));
